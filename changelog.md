0.0.11

* fix cumulative reporting functions

0.0.10

* split out modules
* add search report modules

0.0.9

* Fix `amountReal` to accept function how to handle precision loss.

0.0.8

* Add `amountReal`

0.0.7

* Add `diffDates`

0.0.6

* Fix bug in `realAmount`
* Add `integralAmount`
* Add instances for `Semigroup` and `Monoid`

0.0.5

* Add `encodeTransactionNoEmptyField`

0.0.4

* Use classy lens/prism

0.0.3

* Remove `Combinators` module
* Use `MonadReader` for transaction fields instead of anticipating use of state

0.0.2

* Add combinators for processing bank transactions

0.0.1

* This change log starts
